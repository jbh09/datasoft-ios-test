//
//  SearchVC.swift
//  iOS-Test-DS
//
//  Created by Jamil Bin Hossain on 1/10/21.
//

import UIKit
import Foundation


class SearchVC: UIViewController {
    
    var coordinator: SearchVCCoordinator?
    
    private let activity = ActivityIndicator()
    
    //MARK:- IBoutlet
    @IBOutlet weak var navTitle: UILabel!
    @IBOutlet weak var tableView: UITableView!

    
    //MARK: - View life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
      
        setupViews()
    }
    
    //MARK: - IBActions
    
    
    func setupViews() {
       
    }
}

extension SearchVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return 5
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "checkListCell", for: indexPath)
        
        cell.textLabel?.text = "Hello"
        
        return cell
    }
    
}

