//
//  SearchVCCoordinator.swift
//  iOS-Test-DS
//
//  Created by Jamil Bin Hossain on 1/10/21.
//

import Foundation
import UIKit

final class SearchVCCoordinator: NSObject, Coordinator {
    
    var childCoordinators: [Coordinator] = []
    let navigationController = UINavigationController()
    
    func start() {
        let searchVC: SearchVC = .instantiate(storyBoard: StoryBoard.movies.name)
        searchVC.coordinator = self
        self.navigationController.delegate = self
       
        searchVC.tabBarItem.image = UIImage(named: ItemImage.VendorTabBar.rawValue)
        searchVC.tabBarItem.selectedImage = UIImage(named: SelectedImage.VendorTabBarSelected.rawValue)
        navigationController.navigationBar.isHidden = true
        navigationController.navigationBar.barStyle = .black
        navigationController.pushViewController(searchVC, animated: true)
    }
    
    func popViewController() {
        
    }
}

extension SearchVCCoordinator: UINavigationControllerDelegate{
    
    func navigationController(_ navigationController: UINavigationController, didShow viewController: UIViewController, animated: Bool) {
        
        guard let fromViewController = navigationController.transitionCoordinator?.viewController(forKey: .from) else {
            return
        }
        
        if let vc = fromViewController as? MoviesVC {
            removeChildCoordinator(vc.coordinator!)
        }
    }
    
    func removeChildCoordinator(_ childCoordinator: Coordinator) {
        childCoordinators = childCoordinators.filter { $0 !== childCoordinator }
    }
}

