//
//  Movie.swift
//  iOS-Test-DS
//
//  Created by Jamil Bin Hossain on 2/10/21.
//

import Foundation

struct Movie : Codable {
    
    let id : Int?
    let title : String?   
    let moviePoster : String?

    enum CodingKeys: String, CodingKey {
        case id = "id"
        case title = "title"
        case moviePoster = "poster_path"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        id = try values.decodeIfPresent(Int.self, forKey: .id)
        title = try values.decodeIfPresent(String.self, forKey: .title)
        moviePoster = try values.decodeIfPresent(String.self, forKey: .moviePoster)
    }

}
