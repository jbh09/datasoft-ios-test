//
//  MovieData.swift
//  iOS-Test-DS
//
//  Created by Jamil Bin Hossain on 2/10/21.
//

import Foundation

struct Moviedata : Codable {
    let movie : [Movie]?

    enum CodingKeys: String, CodingKey {

        case movie = "movie"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)

        movie = try values.decodeIfPresent([Movie].self, forKey: .movie)
    }

}
