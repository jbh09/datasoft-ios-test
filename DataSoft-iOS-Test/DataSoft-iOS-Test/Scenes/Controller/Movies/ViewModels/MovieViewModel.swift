//
//  MovieViewModel.swift
//  iOS-Test-DS
//
//  Created by Jamil Bin Hossain on 2/10/21.
//

import Foundation
import UIKit

protocol MovieVCDelegate {
    func reloadData()
}

class MovieViewModel {
    
    private(set) var loaderdelegate: LoaderDelegate?
    private(set) var toastDelegate: ToastDelegate?
    private(set) var movieVCDelegate: MovieVCDelegate?
    
     func viewDidLoad<T>(_ vc: T) {
        self.loaderdelegate = vc.self as? LoaderDelegate
        self.toastDelegate = vc.self as? ToastDelegate
        self.movieVCDelegate = vc.self as? MovieVCDelegate
    }
    
    var item: [Item]
    var movieItems: [MovieItemsVM]
    
    init() {
        self.item = [Item]()
        self.movieItems = [MovieItemsVM]()
    }
    
    func createRequest(limit: Int , offset: Int)-> Resource {
        
        //let urlWithOffset = "\(URL.movieList)?limit=\(limit)&offset=\(offset)"
        
        let urlWithOffset = "\(URL.movieList)?api_key=eb8aa6f914f794f711fb1841fb141f12"
        
        guard let url = URL(string: urlWithOffset) else {
            fatalError("URl was incorrect")
        }
        
        var resource = Resource(url: url)
        resource.httpMethod = HttpMethod.get
        
        return resource
    }
    
    
}

extension MovieViewModel{
    
     func fetchDtaWith(resource: Resource) {
        self.loaderdelegate?.showLoader()
        WebService.load(resource: resource) {[weak self] (result) in
            self?.loaderdelegate?.hideLoader()
     
            switch result {
            
            case .success(let data,let status):
                
                switch status {
                case HTTPStatusCodes.OK:
                    JSONDecoder.decodeData(model: Movie_Response.self, data) { [weak self](result) in
                        switch result
                        {
                        case .success(let data):
                            print(data)
                            self?.convertToViewModel(model: data)
                            break
                        case .failure(let error):
                            print(error)
                            break
                        }
                    }
                    break
                case HTTPStatusCodes.BadRequest:
                    
                    break
                    
                case HTTPStatusCodes.InternalServerError:
                    
                    break
                    
                case HTTPStatusCodes.NotFound:
                    
                    break
                    
                default:
                    break
                    
                }
                break
            case .failure(let error):
                print(error.localizedDescription)
                break
            }
        }
    }
}

extension MovieViewModel{
    
    private func convertToViewModel(model:Movie_Response){
        if let data = model.moviedata {
            self.movieItems = data.map({ (data) -> MovieItemsVM in
              
                var movieVm: [MovieListVM]!
                if let product = data.movie{
                    movieVm = product.map({ (product) -> MovieListVM in
                        return MovieListVM(movie: product)
                    })
                }
                return MovieItemsVM(movies: movieVm)
            })
        }
        
        print("tst",self.movieItems)
        self.movieVCDelegate?.reloadData()
    }
}

struct Item {
    let page: Int?
    let totalPages: Int?
    let totalResults: Int?
    //let movies:[Movies]?
    //let movies:[MovieListVM]?
}

struct MovieItemsVM {
    let movies:[MovieListVM]?
}

//struct Movies {
//    let title: String?
//    let price: String?
//    let url:String?
//}


