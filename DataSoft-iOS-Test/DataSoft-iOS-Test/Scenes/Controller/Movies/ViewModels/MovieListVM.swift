//
//  MovieListVM.swift
//  iOS-Test-DS
//
//  Created by Jamil Bin Hossain on 2/10/21.
//


import Foundation

struct MovieListVM {
    
    let id : Int?
    let title : String?
    let movie_image_url :String?
    
    init(movie : Movie){
        self.id = movie.id
        self.title = movie.title
        self.movie_image_url = movie.moviePoster
    }
}
