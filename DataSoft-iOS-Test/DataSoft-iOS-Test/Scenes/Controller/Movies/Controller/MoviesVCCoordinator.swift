//
//  MoviesVCCoordinator.swift
//  iOS-Test-DS
//
//  Created by Jamil Bin Hossain on 1/10/21.
//

import Foundation
import UIKit

final class MoviesVCCoordinator: NSObject, Coordinator {
    
    var childCoordinators: [Coordinator] = []
    let navigationController = UINavigationController()
    
    func start() {
        let moviesVC: MoviesVC = .instantiate(storyBoard: StoryBoard.movies.name)
        moviesVC.coordinator = self
        self.navigationController.delegate = self
       
        moviesVC.tabBarItem.image = UIImage(named: ItemImage.MyOccasionTabBar.rawValue)
        moviesVC.tabBarItem.selectedImage = UIImage(named: SelectedImage.MyOccasionTabBarSelected.rawValue)
        navigationController.navigationBar.isHidden = true
        navigationController.navigationBar.barStyle = .black
        navigationController.pushViewController(moviesVC, animated: true)
    }
    
    func popViewController() {
        
    }
}

extension MoviesVCCoordinator: UINavigationControllerDelegate{
    
    func navigationController(_ navigationController: UINavigationController, didShow viewController: UIViewController, animated: Bool) {
        
        guard let fromViewController = navigationController.transitionCoordinator?.viewController(forKey: .from) else {
            return
        }
        
        if let vc = fromViewController as? MoviesVC {
            removeChildCoordinator(vc.coordinator!)
        }
    }
    
    func removeChildCoordinator(_ childCoordinator: Coordinator) {
        childCoordinators = childCoordinators.filter { $0 !== childCoordinator }
    }
}

