//
//  MoviesVC.swift
//  iOS-Test-DS
//
//  Created by Jamil Bin Hossain on 1/10/21.
//

import UIKit
import Foundation

class MoviesVC: UIViewController, UITableViewDelegate {
    
    private(set) var movieViewModel = MovieViewModel()
    private(set) var tableViewDataSource: GenericDataSource<MovieTVCell,MovieItemsVM>!
    
    var coordinator: MoviesVCCoordinator?
    private let activity = ActivityIndicator()
    
    //MARK:- IBoutlet
    @IBOutlet weak var navTitle: UILabel!
    @IBOutlet weak var tableView: UITableView!
   
   
    //MARK: - View life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.movieViewModel.viewDidLoad(self)
        tableView.rowHeight = UITableView.automaticDimension
        self.displayHome()
        setupViews()
        fetchMovie()
    }
    
    func setupViews() {
        
    }
    
    private func displayHome(){
        
        self.tableViewDataSource = GenericDataSource.init(cellIdentifier: "MovieTVCell", items: self.movieViewModel.movieItems, configureCell: { (cell, vm,indexpath) in
            cell.movie = vm.movies
            cell.delegate = self
            cell.moviesCollectionView.tag = indexpath.row
            
            cell.moviesCollectionView.reloadData()
            cell.moviesCollectionView.layoutIfNeeded()
           
          
        })
    self.tableView.dataSource = self.tableViewDataSource
}
    
}


//MARK:- MovieTVCell Delegate

extension MoviesVC : MovieTVCellDelegate {
      func selectedItem(itemId:Int){
        
        //coordinator?.gotoProductDetails(id:itemId)
    }
}

extension MoviesVC {
    private func fetchMovie(){
        self.movieViewModel.fetchDtaWith(resource: self.movieViewModel.createRequest(limit: 1000, offset: 1))
    }
}
//MARK:- Toast

extension MoviesVC: LoaderDelegate{
    
    func showLoader() {
        DispatchQueue.main.async { [self] in
            activity.showLoading(view: self.view)
        }
    }
    
    func hideLoader() {
        activity.hideLoading()
    }
}
//MARK:- Loader

extension MoviesVC:ToastDelegate {
    func showToastWith(mess: String) {
        DispatchQueue.main.async {
            ToastView.shared.short(self.view, txt_msg: "  \(mess)  ")
        }
    }
}

extension MoviesVC: MovieVCDelegate {
    func reloadData() {
        self.tableViewDataSource.updateItems(self.movieViewModel.movieItems)
        DispatchQueue.main.async {
            self.tableView.reloadData()
        }
    }
}
