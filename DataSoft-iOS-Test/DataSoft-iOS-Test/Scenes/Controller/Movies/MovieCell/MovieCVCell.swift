//
//  MovieCVCell.swift
//  iOS-Test-DS
//
//  Created by Jamil Bin Hossain on 2/10/21.
//

import UIKit

class MovieCVCell: UICollectionViewCell {
    
    @IBOutlet weak var movieTitleLabel: UILabel!
    
    @IBOutlet weak var moviePosterIV: UIImageView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
}
