//
//  MovieTVCell.swift
//  iOS-Test-DS
//
//  Created by Jamil Bin Hossain on 2/10/21.
//

import UIKit

protocol MovieTVCellDelegate {
    func selectedItem(itemId:Int)
}

let width = UIScreen.main.bounds.width
let leftSpecing:CGFloat = 16
let rightSpecing:CGFloat = 16
let totalSub:CGFloat = 48

class MovieTVCell: UITableViewCell {
    
    var delegate: MovieTVCellDelegate?
    private(set) var movieCollectionViewDataSource: CollectionViewDataSource<MovieCVCell,MovieListVM>!
   
    @IBOutlet weak var moviesCollectionView: DynamicHeightCollectionView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.collectionViewLayout()
        setupViews()
    }
    
    func setupViews() {
        
    }
    
    var movie: [MovieListVM]?{
        didSet{
            guard let movie = movie else {
                return
            }
            self.setRecentProduct(movie)
        }
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
    }
}

extension MovieTVCell {
    
    fileprivate func collectionViewLayout() {
        self.moviesCollectionView.collectionViewLayout = movieCollectionViewLayout()
    }
    
    private func movieCollectionViewLayout() -> UICollectionViewLayout {
        
        let layout = UICollectionViewFlowLayout()
        layout.sectionInset = UIEdgeInsets(top: 0, left: leftSpecing, bottom: 0, right: rightSpecing)
        let secHeight = width / 2
        layout.itemSize = CGSize(width: (width - totalSub) / 2, height: secHeight*1.3)
        layout.minimumInteritemSpacing = 16
        layout.minimumLineSpacing = 16
        
        return layout
    }
}

//MARK:- UICollection && scroll view delegate

extension MovieTVCell: UICollectionViewDelegate {
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
    }
    
//    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
//        if collectionView == self.productsCollectionView{
//            if let product = self.product{
//                if let id = product[indexPath.row].id{
//                    self.delegate?.selectedItem(itemId: id)
//                }
//            }
//        }
//    }
}

////MARK:- Collection view delegate & Datasource
extension MovieTVCell {


    fileprivate func setRecentProduct(_ movie: [MovieListVM]) {
        self.movieCollectionViewDataSource = CollectionViewDataSource(cellIdentifier: "MovieCVCell", items: movie, configureCell: {(cell, vm) in

            cell.movieTitleLabel.text = vm.title
           
            guard let url = vm.movie_image_url else{
                return
            }
            let placeholderImage = UIImage(named: "Recent_product_Placeholder")
            
            
//            cell.moviePosterIV.getImage(url: (url), placeholderImage: placeholderImage) { (success) in
//                cell.moviePosterIV.contentMode = .scaleAspectFill
//                cell.layoutIfNeeded()
//            } failer: { (failed) in
//                print("failed.....")
//                cell.moviePosterIV.image = placeholderImage
//                cell.moviePosterIV.contentMode = .scaleAspectFit
//            }
        })
        self.moviesCollectionView.dataSource = self.movieCollectionViewDataSource
        self.moviesCollectionView.delegate = self
        self.moviesCollectionView.reloadData()

    }
}
