//
//  TVShowsCVCell.swift
//  iOS-Test-DS
//
//  Created by Jamil Bin Hossain on 3/10/21.
//

import UIKit

class TVShowsCVCell: UICollectionViewCell {
    
    @IBOutlet weak var tvShowsTitleLabel: UILabel!
    
    @IBOutlet weak var tvShowsPosterIV: UIImageView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
}
