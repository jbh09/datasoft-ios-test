//
//  TVShowsTVCell.swift
//  iOS-Test-DS
//
//  Created by Jamil Bin Hossain on 3/10/21.
//

import UIKit

protocol TVShowsTVCellDelegate {
    func selectedItem(itemId:Int)
}

let widthTVShow = UIScreen.main.bounds.width
let leftSpecingTVShow:CGFloat = 16
let rightSpecingTVShow:CGFloat = 16
let totalSubTVShow:CGFloat = 48

class TVShowsTVCell: UITableViewCell {
    
    var delegate: TVShowsTVCellDelegate?
    private(set) var tvShowsCollectionViewDataSource: CollectionViewDataSource<TVShowsCVCell,TVShowsListVM>!
   
    @IBOutlet weak var tvShowsCollectionView: DynamicHeightCollectionView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.collectionViewLayout()
        setupViews()
    }
    
    func setupViews() {
        
    }
    
    var tvShow: [TVShowsListVM]?{
        didSet{
            guard let tvShow = tvShow else {
                return
            }
            self.setRecentProduct(tvShow)
        }
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
    }
}

extension TVShowsTVCell {
    
    fileprivate func collectionViewLayout() {
        self.tvShowsCollectionView.collectionViewLayout = movieCollectionViewLayout()
    }
    
    private func movieCollectionViewLayout() -> UICollectionViewLayout {
        
        let layout = UICollectionViewFlowLayout()
        layout.sectionInset = UIEdgeInsets(top: 0, left: leftSpecingTVShow, bottom: 0, right: rightSpecingTVShow)
        let secHeight = widthTVShow / 2
        layout.itemSize = CGSize(width: (widthTVShow - totalSubTVShow) / 2, height: secHeight*1.3)
        layout.minimumInteritemSpacing = 16
        layout.minimumLineSpacing = 16
        
        return layout
    }
}

//MARK:- UICollection && scroll view delegate

extension TVShowsTVCell: UICollectionViewDelegate {
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
    }
    
//    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
//        if collectionView == self.productsCollectionView{
//            if let product = self.product{
//                if let id = product[indexPath.row].id{
//                    self.delegate?.selectedItem(itemId: id)
//                }
//            }
//        }
//    }
}

////MARK:- Collection view delegate & Datasource
extension TVShowsTVCell {


    fileprivate func setRecentProduct(_ movie: [TVShowsListVM]) {
        self.tvShowsCollectionViewDataSource = CollectionViewDataSource(cellIdentifier: "MovieCVCell", items: movie, configureCell: {(cell, vm) in

            cell.tvShowsTitleLabel.text = vm.title
           
            guard let url = vm.tvShows_image_url else{
                return
            }
            let placeholderImage = UIImage(named: "Recent_product_Placeholder")
            
            
//            cell.moviePosterIV.getImage(url: (url), placeholderImage: placeholderImage) { (success) in
//                cell.moviePosterIV.contentMode = .scaleAspectFill
//                cell.layoutIfNeeded()
//            } failer: { (failed) in
//                print("failed.....")
//                cell.moviePosterIV.image = placeholderImage
//                cell.moviePosterIV.contentMode = .scaleAspectFit
//            }
        })
        self.tvShowsCollectionView.dataSource = self.tvShowsCollectionViewDataSource
        self.tvShowsCollectionView.delegate = self
        self.tvShowsCollectionView.reloadData()

    }
}
