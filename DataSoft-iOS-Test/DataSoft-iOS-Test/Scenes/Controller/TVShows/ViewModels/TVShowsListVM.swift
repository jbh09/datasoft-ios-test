//
//  TVShowsListVM.swift
//  iOS-Test-DS
//
//  Created by Jamil Bin Hossain on 3/10/21.
//

import Foundation

struct TVShowsListVM {
    
    let id : Int?
    let title : String?
    let tvShows_image_url :String?
    
    init(tvShow : TVShows){
        self.id = tvShow.id
        self.title = tvShow.title

        if tvShow.tvShowimage_set?.count != 0{
            let image = tvShow.tvShowimage_set
            self.tvShows_image_url = image?[0].image
        }else{
            self.tvShows_image_url = nil
        }
    }
}
