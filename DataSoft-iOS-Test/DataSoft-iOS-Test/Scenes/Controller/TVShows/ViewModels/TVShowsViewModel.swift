//
//  TVShowsViewModel.swift
//  iOS-Test-DS
//
//  Created by Jamil Bin Hossain on 3/10/21.
//

import Foundation
import UIKit

protocol TVShowsVCDelegate {
    func reloadData()
}

class TVShowsViewModel {
    
    private(set) var loaderdelegate: LoaderDelegate?
    private(set) var toastDelegate: ToastDelegate?
    private(set) var movieVCDelegate: TVShowsVCDelegate?
    
     func viewDidLoad<T>(_ vc: T) {
        self.loaderdelegate = vc.self as? LoaderDelegate
        self.toastDelegate = vc.self as? ToastDelegate
        self.movieVCDelegate = vc.self as? TVShowsVCDelegate
    }
    
    var item: [ItemTVShow]
    var tvShowsItems: [TVShowsItemsVM]
    
    init() {
        self.item = [ItemTVShow]()
        self.tvShowsItems = [TVShowsItemsVM]()
    }
    
    func createRequest(limit: Int , offset: Int)-> Resource {
        
        //let urlWithOffset = "\(URL.tvShowList)?limit=\(limit)&offset=\(offset)"
        
        let urlWithOffset = "\(URL.tvShowList)?api_key=eb8aa6f914f794f711fb1841fb141f12"
        
        guard let url = URL(string: urlWithOffset) else {
            fatalError("URl was incorrect")
        }
        
        var resource = Resource(url: url)
        resource.httpMethod = HttpMethod.get
        
        return resource
    }
    
    
}

extension TVShowsViewModel{
    
     func fetchDtaWith(resource: Resource) {
        self.loaderdelegate?.showLoader()
        WebService.load(resource: resource) {[weak self] (result) in
            self?.loaderdelegate?.hideLoader()
     
            switch result {
            
            case .success(let data,let status):
                
                switch status {
                case HTTPStatusCodes.OK:
                    JSONDecoder.decodeData(model: TVShow_Response.self, data) { [weak self](result) in
                        switch result
                        {
                        case .success(let data):
                            print(data)
                            self?.convertToViewModel(model: data)
                            break
                        case .failure(let error):
                            print(error)
                            break
                        }
                    }
                    break
                case HTTPStatusCodes.BadRequest:
                    
                    break
                    
                case HTTPStatusCodes.InternalServerError:
                    
                    break
                    
                case HTTPStatusCodes.NotFound:
                    
                    break
                    
                default:
                    break
                    
                }
                break
            case .failure(let error):
                print(error.localizedDescription)
                break
            }
        }
    }
}

extension TVShowsViewModel{
    
    private func convertToViewModel(model:TVShow_Response){
        if let data = model.tvShowdata {
            self.tvShowsItems = data.map({ (data) -> TVShowsItemsVM in
              
                var movieVm: [TVShowsListVM]!
                if let product = data.tvShow{
                    movieVm = product.map({ (product) -> TVShowsListVM in
                        return TVShowsListVM(tvShow: product)
                    })
                }
                return TVShowsItemsVM(tvShows: movieVm)
            })
        }
        
        print("tst",self.tvShowsItems)
        self.movieVCDelegate?.reloadData()
    }
}

struct ItemTVShow {
    let title: String?
    let tvShows:[TVShow]?
    
}

struct TVShowsItemsVM {
    let tvShows:[TVShowsListVM]?
}

struct TVShow {
    let title: String?
    let price: String?
    let url:String?
}


