//
//  TVShowData.swift
//  iOS-Test-DS
//
//  Created by Jamil Bin Hossain on 3/10/21.
//

import Foundation

struct TVShowData : Codable {
    let tvShow : [TVShows]?

    enum CodingKeys: String, CodingKey {

        case tvShow = "tvShow"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)

        tvShow = try values.decodeIfPresent([TVShows].self, forKey: .tvShow)
    }

}
