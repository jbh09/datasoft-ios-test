//
//  TVShow.swift
//  iOS-Test-DS
//
//  Created by Jamil Bin Hossain on 3/10/21.
//

import Foundation

struct TVShows : Codable {
    
    let id : Int?
    let created : String?
    let title : String?
    let details : String?
    let tvShowimage_set : [TVShow_image_set]?

    enum CodingKeys: String, CodingKey {

        case id = "id"
        case created = "created"
        case title = "title"
        case details = "details"
        case tvShowimage_set = "tvShowimage_set"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        id = try values.decodeIfPresent(Int.self, forKey: .id)
        created = try values.decodeIfPresent(String.self, forKey: .created)
        title = try values.decodeIfPresent(String.self, forKey: .title)
        details = try values.decodeIfPresent(String.self, forKey: .details)
        tvShowimage_set = try values.decodeIfPresent([TVShow_image_set].self, forKey: .tvShowimage_set)
    }

}
