//
//  TVShow_Response.swift
//  iOS-Test-DS
//
//  Created by Jamil Bin Hossain on 3/10/21.
//

import Foundation

struct TVShow_Response : Codable {
    let success : Bool?
    let status_code : Int?
    let message : String?
    let tvShowdata : [TVShowData]?

    enum CodingKeys: String, CodingKey {

        case success = "success"
        case status_code = "status_code"
        case message = "message"
        case tvShowdata = "data"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        success = try values.decodeIfPresent(Bool.self, forKey: .success)
        status_code = try values.decodeIfPresent(Int.self, forKey: .status_code)
        message = try values.decodeIfPresent(String.self, forKey: .message)
        tvShowdata = try values.decodeIfPresent([TVShowData].self, forKey: .tvShowdata)
    }

}
