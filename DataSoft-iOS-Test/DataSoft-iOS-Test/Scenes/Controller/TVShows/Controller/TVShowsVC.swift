//
//  TVShowsVC.swift
//  iOS-Test-DS
//
//  Created by Jamil Bin Hossain on 1/10/21.
//

import UIKit
import Foundation

class TVShowsVC: UIViewController, UITableViewDelegate, TVShowsTVCellDelegate {
    
    private(set) var tvShowsViewModel = TVShowsViewModel()
    private(set) var tableViewDataSource: GenericDataSource<TVShowsTVCell,TVShowsItemsVM>!
    
    var coordinator: TVShowsVCCoordinator?
    private let activity = ActivityIndicator()
    
    //MARK:- IBoutlet
    @IBOutlet weak var navTitle: UILabel!
    @IBOutlet weak var tableView: UITableView!
   
   
    //MARK: - View life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tvShowsViewModel.viewDidLoad(self)
        tableView.rowHeight = UITableView.automaticDimension
        self.displayHome()
        setupViews()
        fetchMovie()
    }
    
    func setupViews() {
        
    }
    
    private func displayHome(){
        
        self.tableViewDataSource = GenericDataSource.init(cellIdentifier: "TVShowsTVCell", items: self.tvShowsViewModel.tvShowsItems, configureCell: { (cell, vm,indexpath) in
            cell.tvShow = vm.tvShows
            cell.delegate = self
            cell.tvShowsCollectionView.tag = indexpath.row
            
            cell.tvShowsCollectionView.reloadData()
            cell.tvShowsCollectionView.layoutIfNeeded()
            
        })
    self.tableView.dataSource = self.tableViewDataSource
}
    
}


//MARK:- MovieTVCell Delegate

extension TVShowsVC : MovieTVCellDelegate {
      func selectedItem(itemId:Int){
        
        //coordinator?.gotoProductDetails(id:itemId)
    }
}

extension TVShowsVC {
    private func fetchMovie(){
        self.tvShowsViewModel.fetchDtaWith(resource: self.tvShowsViewModel.createRequest(limit: 1000, offset: 1))
    }
}
//MARK:- Toast

extension TVShowsVC: LoaderDelegate{
    
    func showLoader() {
        DispatchQueue.main.async { [self] in
            activity.showLoading(view: self.view)
        }
    }
    
    func hideLoader() {
        activity.hideLoading()
    }
}
//MARK:- Loader

extension TVShowsVC:ToastDelegate {
    func showToastWith(mess: String) {
        DispatchQueue.main.async {
            ToastView.shared.short(self.view, txt_msg: "  \(mess)  ")
        }
    }
}

extension TVShowsVC: MovieVCDelegate {
    func reloadData() {
        self.tableViewDataSource.updateItems(self.tvShowsViewModel.tvShowsItems)
        DispatchQueue.main.async {
            self.tableView.reloadData()
        }
    }
}
