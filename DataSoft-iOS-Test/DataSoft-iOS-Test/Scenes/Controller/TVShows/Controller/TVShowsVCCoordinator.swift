//
//  TVShowsVCCoordinator.swift
//  iOS-Test-DS
//
//  Created by Jamil Bin Hossain on 1/10/21.
//

import Foundation
import UIKit

final class TVShowsVCCoordinator: NSObject, Coordinator {
    
    var childCoordinators: [Coordinator] = []
    let navigationController = UINavigationController()
    
    func start() {
        let tvShowsVC: TVShowsVC = .instantiate(storyBoard: StoryBoard.movies.name)
        tvShowsVC.coordinator = self
        self.navigationController.delegate = self
       
        tvShowsVC.tabBarItem.image = UIImage(named: ItemImage.PlanningTabBar.rawValue)
        tvShowsVC.tabBarItem.selectedImage = UIImage(named: SelectedImage.PlanningTabBarSelected.rawValue)
        navigationController.navigationBar.isHidden = true
        navigationController.navigationBar.barStyle = .black
        navigationController.pushViewController(tvShowsVC, animated: true)
    }
    
    func popViewController() {
        
    }
}

extension TVShowsVCCoordinator: UINavigationControllerDelegate{
    
    func navigationController(_ navigationController: UINavigationController, didShow viewController: UIViewController, animated: Bool) {
        
        guard let fromViewController = navigationController.transitionCoordinator?.viewController(forKey: .from) else {
            return
        }
        
        if let vc = fromViewController as? MoviesVC {
            removeChildCoordinator(vc.coordinator!)
        }
    }
    
    func removeChildCoordinator(_ childCoordinator: Coordinator) {
        childCoordinators = childCoordinators.filter { $0 !== childCoordinator }
    }
}

