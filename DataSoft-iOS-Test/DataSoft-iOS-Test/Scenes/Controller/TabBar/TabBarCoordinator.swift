//
//  TabBarCoordinator.swift
//  iOS-Test-DS
//
//  Created by Jamil Bin Hossain on 1/10/21.
//

import Foundation
import UIKit

final class TabBarCoordinator:NSObject, Coordinator,UINavigationControllerDelegate {
    
    //private let keychain = KeychainSwift()
    var childCoordinators: [Coordinator] = []
    private let window: UIWindow
    weak var coordinator: TabBarCoordinator?
    let navigationController = UINavigationController()
    
    private let moviesCoordinator = MoviesVCCoordinator()
    private let tvShowsCoordinator = TVShowsVCCoordinator()
    private let searchCoordinator = SearchVCCoordinator()

    
    private let tabBarController = TabBarVC()
    
    init(window: UIWindow) {
        self.window = window
    }
    
    func start() {
        self.childCoordinators.append(moviesCoordinator)
        moviesCoordinator.start()
        self.childCoordinators.append(tvShowsCoordinator)
        tvShowsCoordinator.start()
        self.childCoordinators.append(searchCoordinator)
        searchCoordinator.start()
     
        tabBarController.coordinator = self
        
        tabBarController.viewControllers = [moviesCoordinator.navigationController,tvShowsCoordinator.navigationController,searchCoordinator.navigationController]
        
        window.rootViewController = tabBarController
        window.makeKeyAndVisible()
    }
    
    func popViewController() {
        
    }
}

extension TabBarCoordinator{
    
    func didFinishChild(coordinator: Coordinator) {
        
        self.removeChildCoordinator(coordinator)
    }
    
    func removeChildCoordinator(_ childCoordinator: Coordinator) {
        childCoordinators = childCoordinators.filter { $0 !== childCoordinator }
    }
}

