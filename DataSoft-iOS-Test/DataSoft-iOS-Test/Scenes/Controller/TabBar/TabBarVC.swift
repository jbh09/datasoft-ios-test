//
//  TabBarVC.swift
//  iOS-Test-DS
//
//  Created by Jamil Bin Hossain on 1/10/21.
//

import UIKit

let homeTab = 0

protocol TabBarVCDelegate {
    func presentAViewController()
}

class TabBarVC: UITabBarController {
    
    
    var tabBarVCDelegate: TabBarVCDelegate?
    //private let keychain = KeychainSwift()
    var coordinator: TabBarCoordinator?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tabBar.layer.masksToBounds = true
        //self.tabBar.isTranslucent = true
        self.tabBar.layer.cornerRadius = 20
        //self.tabBar.layer.shadowColor = UIColor.lightGray.cgColor
        self.tabBar.layer.shadowColor = UIColor.black.cgColor
        self.tabBar.layer.shadowOpacity = 0.3
        self.tabBar.layer.shadowRadius = 3
        
        self.tabBar.layer.shadowOffset = CGSize(width: 2.0, height: 3.0)
        
        self.tabBar.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
    }
}

enum ItemTitle: String {
    case My_Occasion
    case Vendors
    case Planning
    case Quotation
}

enum ItemImage: String {
    case MyOccasionTabBar
    case VendorTabBar
    case PlanningTabBar
    case QuotationTabBar
}

enum SelectedImage: String {
    case MyOccasionTabBarSelected
    case VendorTabBarSelected
    case PlanningTabBarSelected
    case QuotationTabBarSelected
}

