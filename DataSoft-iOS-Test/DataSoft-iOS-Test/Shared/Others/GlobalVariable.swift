//
//  GlobalVariable.swift
//  iOS-Test-DS
//
//  Created by Jamil Bin Hossain on 1/10/21.
//

import Foundation

class GlobalVariable {
    
    static var shared = GlobalVariable()
    
    //var user: Users?
    var token: String?
    //var countryList: [CountryPreference]?
    //var cityList: [CityPreference]?
    
}

extension GlobalVariable{
    
    func getJwt() -> String? {
        
        if let token = self.token{
            return "Bearer" + " " + "\(token)"

        }else{
            return nil
        }
    }
    
    class func clearGlobalVariable() {
        shared = GlobalVariable()
    }
}
