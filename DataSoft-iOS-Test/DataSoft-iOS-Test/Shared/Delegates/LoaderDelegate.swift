//
//  LoaderDelegate.swift
//  iOS-Test-DS
//
//  Created by Jamil Bin Hossain on 2/10/21.
//

import Foundation

protocol LoaderDelegate {
    func showLoader()
    func hideLoader()
}
