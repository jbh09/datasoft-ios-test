//
//  URL+Urls.swift
//  iOS-Test-DS
//
//  Created by Jamil Bin Hossain on 30/9/21.
//

import Foundation

extension URL{
    #if QA
    static let baseUrl  = "https://api.themoviedb.org/3"
    #else
    static let baseUrl  = "https://api.themoviedb.org/3"
    #endif
}

extension URL {

    static let movieList     = "\(URL.baseUrl)/discover/movie/"
    static let tvShowList    = "\(URL.baseUrl)/discover/tv/"
    static let movieSearch   = "\(URL.baseUrl)/search/movie/"
    static let tvShowSearch  = "\(URL.baseUrl)/search/tv/"
    static let movieDetails  = "\(URL.baseUrl)/movie/"
    static let tvShowDetails = "\(URL.baseUrl)/tv/"
}
