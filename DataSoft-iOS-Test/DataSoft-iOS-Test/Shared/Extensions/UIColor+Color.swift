//
//  UIColor+Color.swift
//  iOS-Test-DS
//
//  Created by Jamil Bin Hossain on 1/10/21.
//

import Foundation
import UIKit

extension UIColor {
    
    static let borderColor = UIColor(displayP3Red: 204/255, green: 204/255, blue: 204/255, alpha: 1.0)
    
    static let backgroundColor = UIColor(displayP3Red: 30/255, green: 51/255, blue: 109/255, alpha: 1.0)
    static let cellColor = UIColor(displayP3Red: 20/255, green: 180/255, blue: 200/255, alpha: 1.0)
    
    static let primaryColor = UIColor(red: 141/255, green: 19/255, blue: 30/255, alpha: 1.0).cgColor
    
    
    static let tabCellColor = UIColor(displayP3Red: 241/255, green: 241/255, blue: 241/255, alpha: 1.0)
    
    static let textColorOnExpandableViewCell = UIColor(displayP3Red: 79/255, green: 79/255, blue: 79/255, alpha: 1.0)
    static let customPopUpBackgroundColor = UIColor(displayP3Red: 0/255, green: 0/255, blue: 0/255, alpha: 0.50)
    static let navigationColor = UIColor(displayP3Red: 158/255, green: 0/255, blue: 93/255, alpha: 1.0)
    static let sizeButtonColor = UIColor(displayP3Red: 204/255, green: 204/255, blue: 204/255, alpha: 1.0)
    
    
    
}

extension UIColor {
    public convenience init?(hex: String) {
        let r, g, b, a: CGFloat

        if hex.hasPrefix("#") {
            let start = hex.index(hex.startIndex, offsetBy: 1)
            let hexColor = String(hex[start...])

            if hexColor.count == 8 {
                let scanner = Scanner(string: hexColor)
                var hexNumber: UInt64 = 0

                if scanner.scanHexInt64(&hexNumber) {
                    r = CGFloat((hexNumber & 0xff000000) >> 24) / 255
                    g = CGFloat((hexNumber & 0x00ff0000) >> 16) / 255
                    b = CGFloat((hexNumber & 0x0000ff00) >> 8) / 255
                    a = CGFloat(hexNumber & 0x000000ff) / 255

                    self.init(red: r, green: g, blue: b, alpha: a)
                    return
                }
            }
        }

        return nil
    }
}
