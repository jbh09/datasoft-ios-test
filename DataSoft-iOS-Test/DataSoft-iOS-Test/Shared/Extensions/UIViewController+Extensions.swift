//
//  UIViewController+Extensions.swift
//  iOS-Test-DS
//
//  Created by Jamil Bin Hossain on 1/10/21.
//

import UIKit

extension UIViewController {
    
    static func instantiate<T>(storyBoard: String) -> T {
        let storyboard = UIStoryboard(name: storyBoard, bundle: .main)
        let controller = storyboard.instantiateViewController(identifier: "\(T.self)") as! T
        return controller
    }
}

enum StoryBoard: String {
    
    case movies = "Movies"
    
    var name:String {
        return self.rawValue
    }
}
